<?php

namespace App\Http\Controllers;

use App\ClientProcess;
use App\Office;
use App\Process;
use Illuminate\Http\Request;
use App\Step;

class ProcessController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        $processes = Process::with('office.area.region.division')->where('process_type_id', '=', $process_type_id);

        if ($request->has('q')) {
            $processes->where('name', 'LIKE', "%" . $request->input('q') . "%");
        }

        $parameters = [
            'processes' => $processes->get(),
            'type_name' => $process_type_id == 1 ? 'Processes' : 'Related Parties Structure',
            'processes' => $processes->get(),
            'process_type_id' => $process_type_id
        ];

        return view('processes.index')->with($parameters);
    }

    public function show(Process $process, Request $request)
    {
        $process = Process::with(['steps'=> function ($q){
            $q->whereNull('deleted_at');
        }],'office.area.region.division')->where('id',$process->id)->first();

        $process_type_id = $request->has('t') ? $request->input('t'):1;

        return view('processes.show')->with(['process' => $process, 'process_type_id' => $process_type_id]);
    }

    public function create(Request $request)
    {
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        $parameters = [
            'offices' => Office::orderBy('name')->get()->pluck('name', 'id'),
            'type_name' => $process_type_id == 1 ? 'Processes' : 'Related Parties Structure',
            'process_type_id' => $process_type_id
        ];

        return view('processes.create')->with($parameters);
    }

    public function store(Request $request)
    {
        $process = new Process;
        $process->name = $request->input('name');
        $process->office_id = $request->input('office');
        $process->not_started_colour = str_replace('#', '', $request->input('not_started_colour'));
        $process->started_colour = str_replace('#', '', $request->input('started_colour'));
        $process->completed_colour = str_replace('#', '', $request->input('completed_colour'));
        $process->office_id = auth()->user()->office()->id;
        $process->process_type_id = $request->has('process_type_id') ? $request->input('process_type_id') : 1;
        $process->save();

        $type_name = $request->has('process_type_id') && $request->input('process_type_id') == 2 ? 'Related Parties Structure' : 'Process';

        return redirect(route('processes.show',$process))->with('flash_success', $type_name.' create successfully.');
    }

    public function edit(Process $process)
    {
        $paramaters = [
            'process' => $process,
            'offices' => Office::orderBy('name')->get()->pluck('name', 'id'),
        ];

        return view('processes.edit')->with($paramaters);
    }

    public function update(Process $process, Request $request)
    {
        $process->name = $request->input('name');
        $process->office_id = $request->input('office');
        $process->not_started_colour = str_replace('#', '', $request->input('not_started_colour'));
        $process->started_colour = str_replace('#', '', $request->input('started_colour'));
        $process->completed_colour = str_replace('#', '', $request->input('completed_colour'));
        $process->save();

        return redirect(route('processes.show',$process))->with('flash_success', 'Process updated successfully.');
    }

    public function destroy(Process $process, $processid, Request $request){

        if($process->clients->count()>0){

            return redirect(route('processes.show',$process))->with('flash_danger', 'There are still active clients assigned to that process.');
        }

        $process->destroy($processid);

        $process_type_id = $request->has('t') ? $request->input('t') : 1;
        $type_name = $process_type_id == 2 ? 'Related Parties Structure' : 'Process';

        return redirect(route('processes.index', ['t' => $process_type_id]))->with(['flash_success' => $type_name.'deleted successfully.']);
    }

    public function processStepCount($process_id){
        $count = Step::where('process_id',$process_id)->count();

        return response()->json($count);
    }

    public function getProcessFirstStep(Request $request, $clientid,$processid){

        //$first_step = Step::where('process_id',$processid)->orderBy('order')->first();
        $first_step = ClientProcess::where('process_id',$processid)->where('client_id',$clientid)->first();

        return response()->json($first_step['step_id']);
    }
}