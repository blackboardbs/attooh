<?php

namespace App\Http\Controllers;

use App\Activity;
use App\ActivityInClientBasket;
use App\FormInputDropdownItem;
use App\FormInputRadioItem;
use App\FormInputCheckboxItem;
use App\FormLog;
use App\Forms;
use App\FormSection;
use App\FormSectionInputInClientBasket;
use App\FormSectionInputs;
use App\User;
use Illuminate\Http\Request;
use App\Client;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Step;
use App\Log;
use App\FormInputTextData;
use App\FormInputTextareaData;
use App\FormInputDropdownData;
use App\FormInputDateData;
use App\FormInputBooleanData;
use App\FormInputCheckboxData;
use App\FormInputRadioData;
use App\ClientForm;
use PhpOffice\PhpWord\Writer\PDF\DomPDF;
use PhpOffice\PhpWord\Settings;
use Illuminate\Support\Facades\Response;

class FormsController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
        //return $this->middleware('auth')->except('index');
    }

    public function index(Request $request){

        $forms = Forms::get();

        $parameters = [
            'forms' => $forms
        ];

        return view('forms.index')->with($parameters);
    }

    public function create(Request $request){
        return view('forms.create');
    }

    public function store(Request $request){
        $forms = new Forms;
        $forms->name = $request->input('name');
        $forms->save();

        return redirect(route('forms.show',$forms->id))->with('flash_success', 'Form created successfully');
    }

    public function edit($form){

        $form = Forms::find($form);

        $formfields = FormSection::with('form_section_input.input')->where('form_id',$form->id)->get();

        //dd($formfields);

        $formfield_array=[];
        foreach($formfields as $formfield){
            $formfield_array[$formfield->name]=array();
            foreach ($formfield->form_section_input as $form_section_input){
                //dd($form_section_input->name);
                //array_push($formfield_array[$formfield->name], ['name' => $form_section_input->name, 'type' => $form_section_input->input_type]);
                switch ($form_section_input->input_type) {
                    case 'App\FormInputDropdown':
                        $items = FormInputDropdownItem::select('name')->where('form_input_dropdown_id', $form_section_input->input_id)->get()->toArray();
                        array_push($formfield_array[$formfield->name], ['name' => $form_section_input->name, 'order' => $form_section_input->order,'items'=>$items]);

                        break;
                    case 'App\FormInputRadio':
                        $items = FormInputRadioItem::select('name')->where('form_input_radio_id', $form_section_input->input_id)->get()->toArray();
                        array_push($formfield_array[$formfield->name], ['name' => $form_section_input->name, 'order' => $form_section_input->order,'items'=>$items]);
                        break;
                    case 'App\FormInputCheckbox':
                        $items = FormInputCheckboxItem::select('name')->where('form_input_checkbox_id', $form_section_input->input_id)->get()->toArray();
                        array_push($formfield_array[$formfield->name], ['name' => $form_section_input->name, 'order' => $form_section_input->order,'items'=>$items]);
                        break;
                    default:
                        $items = FormInputDropdownItem::select('name')->where('form_input_dropdown_id', $form_section_input->input_id)->get()->toArray();
                        array_push($formfield_array[$formfield->name], ['name' => $form_section_input->name, 'order' => $form_section_input->order]);
                        break;
                }

            }
        }

        //dd($formfield_array);

        $paramaters = [
            'form' => $form,
            'form_sections' => FormSection::where('form_id',$form->id)->orderBy('order')->pluck('name','id')->prepend('Please Select','0'),
            'formfields' => $formfield_array
        ];

        return view('forms.edit')->with($paramaters);
    }

    public function uploadTemplate($form){

        $form = Forms::find($form);

        $formfields = FormSection::with('form_section_input.input.data')->where('form_id',$form->id)->get();

        //dd($formfields);

        $paramaters = [
            'form' => $form,
            'form_sections' => FormSection::where('form_id',$form->id)->orderBy('order')->pluck('name','id')->prepend('Please Select','0'),
            'formfields' => $formfields
        ];

        return view('forms.template')->with($paramaters);
    }

    public function update($formid,Request $request){
        $form = Forms::find($formid);
        if ($request->has('name')) {
            $form->name = $request->input('name');
        }
        if ($request->has('signature')) {
            $form->signature = $request->input('signature');
        }
        if ($request->hasFile('template_file')) {
            //$request->file('file')->store('templates');
            //ToDo: The above save every file as .bin, Please fix if you have a better way of uploading documents
            $file = $request->file('template_file');
            $file->storeAs('forms/templates',$file->getClientOriginalName());
            $uploadedfile = $file->getClientOriginalName();
            /*$stored = $file->storeAs('forms/templates', $name);*/
            $form->file = $uploadedfile;
        }


        $form->save();

        return redirect(route('forms.show',$form))->with('flash_success', 'Form updated successfully.');
    }

    public function show(Forms $form){

        $formfields = FormSection::with('form_section_input.input.data')->where('form_id',$form->id)->get();

        $parameters = [
            'forms' => $form->load('sections'),
            'form_sections' => FormSection::where('form_id',$form->id)->orderBy('order')->pluck('name','id')->prepend('Please Select','0'),
            'formfields' => $formfields
        ];

        return view('forms.show')->with($parameters);
    }

    public function destroy($formid){

        Forms::destroy($formid);

        return redirect(route('forms.index'))->with('flash_success', 'Form deleted successfully.');
    }

    public function DynamicForm(Client $client,$form_id,$section_id){

        /*$client = Client::find($client_id);*/

        if($section_id == 0) {
            $client_forms = ClientForm::where('id',$form_id)->first();
            $form = Forms::withTrashed()->where('id',$client_forms->dynamic_form_id)->first();
            $form_step_first = FormSection::where('form_id',$client_forms->dynamic_form_id)->orderBy('order')->first()->id;
            $form_section = FormSection::find($form_step_first);
        } else {
            $form = Forms::withTrashed()->where('id',$form_id)->first();
            $form_section = FormSection::find($section_id);
        }

        $step = Step::withTrashed()->find(1);
        $process_progress = $client->getProcessStepProgress($step);
        $form_progress = $client->getFormsStepProgress($form_section);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($c_step_order->order == $a_step->order)
                $progress_color = $client->process->getStageHex(1);

            if($c_step_order->order > $a_step->order)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($step_data, $tmp_step);

        endforeach;

        if($section_id == 0) {
            $client_forms = ClientForm::where('id', $form_id)->first();
            $sections = FormSection::where('form_id', $client_forms->dynamic_form_id)->orderBy('order', 'asc')->get();
        } else {
            $sections = FormSection::where('form_id', $form_id)->orderBy('order', 'asc')->get();
        }

        $form_data = [];
        foreach ($sections as $a_step):
            $progress_color = $client->process->getStageHex(0);

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'form_id' => $a_step->form_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($form_data, $tmp_step);

        endforeach;


        return view('clients.forms.form')->with([
            'client' => $client,
            'step'=>$step,
            'active' => $step,
            'process_progress' => $process_progress,
            'form_progress' => $form_progress,
            'form_section' => $form_section,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'forms' => $form_data,
            'form' => $form
        ]);
    }

    public function storeDynamicForm(Client $client, Request $request){
        //dd($request);
        if($request->has('form_section_id') && $request->input('form_section_id') != ''){
            $log = new Log;
            $log->client_id = $client->id;
            $log->user_id = auth()->id();
            $log->save();

            $id = $client->id;
            $form_section = FormSection::find($request->input('form_section_id'));
            $form_section = $form_section->load(['form_section_inputs.input.data' => function ($query) use ($id) {
                $query->where('client_id', $id);
            }]);

            $all_activities_completed = false;
            foreach ($form_section->form_section_inputs as $activity) {
                if(is_null($request->input($activity->id))){
                    if($request->input('old_'.$activity->id) != $request->input($activity->id)){

                        if(is_array($request->input($activity->id))){

                            $old = explode(',',$request->input('old_'.$activity->id));
                            $diff = array_diff($old,$request->input($activity->id));
                            //dd($diff);

                            foreach($request->input($activity->id) as $key => $value) {
                                $activity_log = new FormLog;
                                $activity_log->log_id = $log->id;
                                $activity_log->input_id = $activity->id;
                                $activity_log->input_name = $activity->name;
                                $activity_log->old_value = $request->input('old_' . $activity->id);
                                $activity_log->new_value = $value;
                                $activity_log->save();
                            }
                        } else {
                            $old = $request->input('old_'.$activity->id);

                            $activity_log = new FormLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->input_id = $activity->id;
                            $activity_log->input_name = $activity->name;
                            $activity_log->old_value = $request->input('old_'.$activity->id);
                            $activity_log->new_value = $request->input($activity->id);
                            $activity_log->save();
                        }

                        switch ($activity->actionable_type) {
                            case 'App\FormInputBoolean':
                                FormInputBooleanData::where('input_boolean_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputDate':
                                FormInputDateData::where('input_date_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputText':
                                FormInputTextData::where('input_text_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputTextarea':
                                FormInputTextareaData::where('input_textarea_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputDropdown':
                                FormInputDropdownData::where('input_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputRadio':
                                FormInputRadioData::where('input_radio_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            case 'App\FormInputCheckbox':
                                FormInputCheckboxData::where('input_dropdown_id',$activity->actionable_id)->where('client_id',$client->id)->delete();

                                break;
                            default:
                                //todo capture defaults
                                break;
                        }
                    }
                }

                if ($request->has($activity->id) && !is_null($request->input($activity->id))) {
                    //If value did not change, do not save it again or add it to log
                    if ($request->input('old_' . $activity->id) == $request->input($activity->id)) {
                        continue;
                    }
                    if(is_array($request->input($activity->id))){

                        $old = explode(',',$request->input('old_'.$activity->id));
                        $diff = array_diff($old,$request->input($activity->id));
                        //dd($diff);

                        foreach($request->input($activity->id) as $key => $value) {
                            $activity_log = new FormLog;
                            $activity_log->log_id = $log->id;
                            $activity_log->input_id = $activity->id;
                            $activity_log->input_name = $activity->name;
                            $activity_log->old_value = $request->input('old_' . $activity->id);
                            $activity_log->new_value = $value;
                            $activity_log->save();
                        }
                    } else {
                        $old = $request->input('old_'.$activity->id);

                        $activity_log = new FormLog;
                        $activity_log->log_id = $log->id;
                        $activity_log->input_id = $activity->id;
                        $activity_log->input_name = $activity->name;
                        $activity_log->old_value = $request->input('old_'.$activity->id);
                        $activity_log->new_value = $request->input($activity->id);
                        $activity_log->save();
                    }

                    //activity type hook
                    //dd($request);
                    switch ($activity->input_type) {
                        case 'App\FormInputBoolean':
                            FormInputBooleanData::where('client_id',$client->id)->where('Form_input_boolean_id',$activity->input_id)->where('data',$old)->delete();

                            FormInputBooleanData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_boolean_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputDate':
                            FormInputDateData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_date_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputText':

                            FormInputTextData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_text_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputTextarea':

                            FormInputTextareaData::insert([
                                'data' => $request->input($activity->id),
                                'form_input_textarea_id' => $activity->input_id,
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);
                            break;
                        case 'App\FormInputDropdown':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    FormInputDropdownData::insert([
                                        'form_input_dropdown_id' => $activity->input_id,
                                        'form_input_dropdown_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    FormInputDropdownData::where('client_id',$client->id)->where('form_input_dropdown_id',$activity->input_id)->whereIn('form_input_dropdown_item_id',$diff)->delete();
                                }
                            }
                            break;
                        case 'App\FormInputCheckbox':
                            foreach($request->input($activity->id) as $key => $value){
                                if(in_array($value,$old,true)) {

                                } else {
                                    FormInputCheckboxData::insert([
                                        'form_input_checkbox_id' => $activity->input_id,
                                        'form_input_checkbox_item_id' => $value,
                                        'client_id' => $client->id,
                                        'user_id' => auth()->id(),
                                        'duration' => 120,
                                        'created_at' => now()
                                    ]);
                                }

                                if(!empty($diff)){
                                    FormInputCheckboxData::where('client_id',$client->id)->where('form_input_checkbox_id',$activity->input_id)->whereIn('form_input_checkbox_item_id',$diff)->delete();
                                }
                            }
                            break;
                        case 'App\FormInputRadio':
                            //foreach($request->input($activity->id) as $key => $value){
                            //dd($old);
                            FormInputRadioData::where('client_id',$client->id)->where('form_input_radio_id',$activity->input_id)->delete();
                            FormInputRadioData::insert([
                                'form_input_radio_id' => $activity->input_id,
                                'form_input_radio_item_id' => $request->input($activity->id),
                                'client_id' => $client->id,
                                'user_id' => auth()->id(),
                                'duration' => 120,
                                'created_at' => now()
                            ]);


                            /*if(!empty($diff)){
                                FormInputRadioData::where('client_id',$client->id)->where('form_input_radio_id',$activity->input_id)->whereIn('form_input_radio_item_id',$diff)->delete();
                            }*/
                            //}
                            break;
                        default:
                            //todo capture defaults
                            break;
                    }

                }
            }
        }

        $form = Forms::withTrashed()->where('id',$form_section->form_id)->first();

        $client_form = ClientForm::where('dynamic_form_id',$form->id)->where('client_id',$client->id)->get();

        if(count($client_form) == 0) {
            $document = new ClientForm();
            $document->name = $form->name;
            $document->form_type = $form->name;
            //$document->file = $form->file;
            $document->file = str_replace(' ','_',$form->name). "_" . ($client->company != null ? str_replace(' ','_',$client->company) : $client->first_name.'_'.$client->last_name) . ".pdf";
            $document->user_id = auth()->id();
            $document->dynamic_form = '1';
            $document->dynamic_form_id = $form->id;
            $document->client_id = $client->id;

            $document->save();
        }

        $filename = str_replace(' ','_',$form->name). "_" . ($client->company != null ? str_replace(' ','_',$client->company) : $client->first_name.'_'.$client->last_name) . ".pdf";

        if(isset($document->id)){
            return redirect()->back()->with(['flash_success' => 'Form details captured.<br /><a href="'.route('forms.process',['client_id'=>$client->id,'form_id'=>$document->id]).'" target="_blank">'.$filename.'</a>']);
        } else {
            $document = ClientForm::where('client_id',$client->id)->where('dynamic_form_id',$form->id)->first()->id;
            return redirect()->back()->with(['flash_success' => 'Form details captured.<br /><a href="'.route('forms.process',['client_id'=>$client->id,'form_id'=>$document]).'" target="_blank">'.$filename.'</a>']);
        }

    }

    public function editDynamicForm(Client $client,$form_id,$section_id){
//create dynamic form entry
//        $client = Client::find($client_id);

        $client_form = ClientForm::where('id',$form_id)->first();
//dd($form_id);
        $form = Forms::where('id',$client_form->dynamic_form_id)->first();

        $form_step_first = FormSection::where('form_id',$client_form->dynamic_form_id)->orderBy('order')->first()->id;

        $form_section = FormSection::find($form_step_first);

        $step = Step::withTrashed()->find($client->step_id);

        $process_progress = $client->getProcessStepProgress($step);
        $form_progress = $client->getFormsStepProgress($form_section);

        $client_progress = $client->process->getStageHex(0);

        if($client->step_id == $step->id)
            $client_progress = $client->process->getStageHex(1);

        if($client->step_id > $step->id)
            $client_progress = $client->process->getStageHex(2);

        $steps = Step::where('process_id', $client->process_id)->orderBy('order','asc')->get();
        $c_step_order = Step::where('id',$client->step_id)->withTrashed()->first();

        $step_data = [];
        foreach ($steps as $a_step):
            $progress_color = $client->process->getStageHex(0);

            if($c_step_order->order == $a_step->order)
                $progress_color = $client->process->getStageHex(1);

            if($c_step_order->order > $a_step->order)
                $progress_color = $client->process->getStageHex(2);


            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'process_id' => $a_step->process_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($step_data, $tmp_step);

        endforeach;

        $sections = FormSection::where('form_id',$client_form->dynamic_form_id)->orderBy('order','asc')->get();

        $form_data = [];
        foreach ($sections as $a_step):
            $progress_color = $client->process->getStageHex(0);

            $tmp_step = [
                'id' => $a_step->id,
                'name' => $a_step->name,
                'form_id' => $a_step->form_id,
                'progress_color' => $progress_color,
                'order' => $a_step->order
            ];

            array_push($form_data, $tmp_step);

        endforeach;


        return view('clients.forms.form')->with([
            'client' => $client,
            'step'=>$step,
            'active' => $step,
            'process_progress' => $process_progress,
            'form_progress' => $form_progress,
            'form_section' => $form_section,
            'view_process_dropdown' => $client->clientProcessIfActivitiesExist(),
            'steps' => $step_data,
            'forms' => $form_data,
            'form' => $form
        ]);
    }

    public function deleteDynamicForm(Client $client,$form){

    }

    public function processDynamicForm($client_id, $form_id)
    {

        /*$client = Client::find($client_id);*/

        $client_form = ClientForm::where('id',$form_id)->where('client_id',$client_id)->first();

        $template_file = Forms::withTrashed()->where('id',$client_form['dynamic_form_id'])->first()->file;

        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext == "pdf") {
            return $this->processPdf($client_id,$form_id);
        } elseif($ext == "docx") {
            return $this->processDocx($client_id,$form_id);
        } else {
            return $template_file;
        }

    }

    public function viewDynamicForm($client_id, $form_id)
    {

        /*$client = Client::find($client_id);*/

        $client_form = ClientForm::where('id',$form_id)->where('client_id',$client_id)->first();

        $template_file = Forms::withTrashed()->where('id',$client_form->dynamic_form_id)->first()->file;

        $ext = pathinfo($template_file, PATHINFO_EXTENSION);


        return Storage::download(storage_path('app/forms/templates/' .$template_file));

    }

    public function processDocx($client_id,$form_id){
        $client = Client::find($client_id);

        $client_form = ClientForm::where('id',$form_id)->where('client_id',$client_id)->first();

        $template_file = Forms::withTrashed()->where('id',$client_form['dynamic_form_id'])->first()->file;

        $formfields = FormSection::with(['form_section_input.input.data'=>function ($q) use ($client_id){
            $q->where('client_id',$client_id);
        }])->where('form_id',$client_form->dynamic_form_id)->get();

        if($client_form->signed == '1') {
            $user = User::where('id',$client_form->signed_by)->first();
        }
        $templateProcessor = new TemplateProcessor(storage_path('app/forms/templates/' . $template_file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $var_array = array();
        $value_array = array();
        //dd($formfields);
        array_push($var_array, 'client');
        array_push($value_array, ($client->company != null ? $client->company : $client->first_name . ' ' . $client->last_name));
        foreach($formfields as $sections) {
            foreach ($sections["form_section_input"] as $section) {
                if($client_form->signed == '1') {
                    $var = '';
                    switch ($section['input_type']){
                        case 'App\FormInputDropdown':
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            $items = FormInputDropdownItem::where('form_input_dropdown_id', $section->input_id)->get();
                            foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }
                            break;
                        case 'App\FormInputRadio':
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            $items = FormInputRadioItem::where('form_input_radio_id', $section->input_id)->get();
                            foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }
                            break;
                        case 'App\FormInputCheckbox':
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            $items = FormInputCheckboxItem::where('form_input_checkbox_id', $section->input_id)->get();
                            foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }
                            break;
                        default:
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            array_push($var_array, $var);
                            break;
                    }


                    if (isset($section["input"]->data) && count($section["input"]->data) > 0) {
                        foreach ($section["input"]->data as $value) {

                            switch ($section['input_type']){
                                case 'App\FormInputDropdown':
                                    $items = FormInputDropdownItem::where('form_input_dropdown_id',$value->form_input_dropdown_id)->get();
                                    if($item){
                                        foreach ($items as $item) {
                                            $data = FormInputDropdownData::where('client_id',$client_id)->where('form_input_dropdown_item_id', $value->form_input_dropdown_item_id)->first();
                                            if($data && $data["form_input_dropdown_item_id"] == $item->id){
                                                array_push($value_array, $item['name']);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                case 'App\FormInputRadio':
                                    $items = FormInputRadioItem::where('form_input_radio_id',$value->form_input_radio_id)->get();
                                    if($item){
                                        foreach ($items as $item) {
                                            $data = FormInputRadioData::where('client_id',$client_id)->where('form_input_radio_item_id', $value->form_input_radio_item_id)->first();
                                            if($data && $data["form_input_radio_item_id"] == $item->id){
                                                array_push($value_array, $item['name']);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                case 'App\FormInputCheckbox':
                                    $items = FormInputCheckboxItem::where('form_input_checkbox_id',$value->form_input_checkbox_id)->get();
                                    if($item){
                                        foreach ($items as $item) {
                                            $data = FormInputCheckboxData::where('client_id',$client_id)->where('form_input_checkbox_item_id', $value->form_input_checkbox_item_id)->first();
                                            if($data && $data["form_input_checkbox_item_id"] == $item->id){
                                                array_push($value_array, $item['name']);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                default:

                                    array_push($value_array, $value->data);
                                    break;
                            }
                        }
                    } else {

                        //dd($section["input_type"]);
                        switch ($section['input_type']){
                            case 'App\FormInputDropdown':
                                $items = FormInputDropdownItem::where('form_input_dropdown_id',$section["input_id"])->get();
                                if($item){
                                    foreach ($items as $item) {

                                        array_push($value_array, '');

                                    }
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            case 'App\FormInputRadio':
                                $items = FormInputRadioItem::where('form_input_radio_id',$section["input_id"])->get();
                                if($item){
                                    foreach ($items as $item) {

                                        array_push($value_array, '');

                                    }
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            case 'App\FormInputCheckbox':
                                $datas = FormInputCheckboxData::where('client_id',$client_id)->where('form_input_checkbox_id', $value->form_input_checkbox_id)->get();
                                //dd($datas);
                                foreach ($datas as $data) {
                                    if (isset($data)) {
                                        $value = FormInputCheckboxItem::where('id', $data->form_input_checkbox_item_id)->first();
                                        if($value['form_input_checkbox_id'] == $data->form_input_checkbox_id) {
                                            if(!in_array($value['name'],$value_array)) {
                                                array_push($value_array, $value['name']);
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }
                                }
                                break;
                            default:

                                array_push($value_array, '');
                                break;
                        }
                    }

                } else {

                    $var = '';
                    switch ($section['input_type']){
                        case 'App\FormInputDropdown':
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            $items = FormInputDropdownItem::where('form_input_dropdown_id', $section->input_id)->get();
                            foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }
                            break;
                        case 'App\FormInputRadio':
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            $items = FormInputRadioItem::where('form_input_radio_id', $section->input_id)->get();
                            foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }
                            break;
                        case 'App\FormInputCheckbox':
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            $items = FormInputCheckboxItem::where('form_input_checkbox_id', $section->input_id)->get();
                            foreach($items as $item) {
                                array_push($var_array, $var . '.' . str_replace(' ', '_', strtolower(str_replace(' ', '_', $item->name))));
                            }
                            break;
                        default:
                            $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                            array_push($var_array, $var);
                            break;
                    }

                    if (isset($section["input"]->data) && count($section["input"]->data) > 0) {
                        foreach ($section["input"]->data as $value) {

                            switch ($section['input_type']){
                                case 'App\FormInputDropdown':
                                    $items = FormInputDropdownItem::where('form_input_dropdown_id',$value->form_input_dropdown_id)->get();
                                    if($item){
                                        foreach ($items as $item) {
                                            $data = FormInputDropdownData::where('client_id',$client_id)->where('form_input_dropdown_item_id', $value->form_input_dropdown_item_id)->first();
                                            if($data && $data["form_input_dropdown_item_id"] == $item->id){
                                                array_push($value_array, $item['name']);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                case 'App\FormInputRadio':
                                    $items = FormInputRadioItem::where('form_input_radio_id',$value->form_input_radio_id)->get();
                                    if($item){
                                        foreach ($items as $item) {
                                            $data = FormInputRadioData::where('client_id',$client_id)->where('form_input_radio_item_id', $value->form_input_radio_item_id)->first();
                                            if($data && $data["form_input_radio_item_id"] == $item->id){
                                                array_push($value_array, $item['name']);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                case 'App\FormInputCheckbox':
                                    $items = FormInputCheckboxItem::where('form_input_checkbox_id',$value->form_input_checkbox_id)->get();
                                    if($item){
                                        foreach ($items as $item) {
                                            $data = FormInputCheckboxData::where('client_id',$client_id)->where('form_input_checkbox_item_id', $value->form_input_checkbox_item_id)->first();
                                            if($data && $data["form_input_checkbox_item_id"] == $item->id){
                                                array_push($value_array, $item['name']);
                                            } else {
                                                array_push($value_array, '');
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }

                                    break;
                                default:

                                    array_push($value_array, $value->data);
                                    break;
                            }
                        }
                    } else {

                        //dd($section["input_type"]);
                        switch ($section['input_type']){
                            case 'App\FormInputDropdown':
                                $items = FormInputDropdownItem::where('form_input_dropdown_id',$section["input_id"])->get();
                                if($item){
                                    foreach ($items as $item) {

                                        array_push($value_array, '');

                                    }
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            case 'App\FormInputRadio':
                                $items = FormInputRadioItem::where('form_input_radio_id',$section["input_id"])->get();
                                if($item){
                                    foreach ($items as $item) {

                                        array_push($value_array, '');

                                    }
                                } else {
                                    array_push($value_array, '');
                                }

                                break;
                            case 'App\FormInputCheckbox':
                                $datas = FormInputCheckboxData::where('client_id',$client_id)->where('form_input_checkbox_id', $value->form_input_checkbox_id)->get();
                                //dd($datas);
                                foreach ($datas as $data) {
                                    if (isset($data)) {
                                        $value = FormInputCheckboxItem::where('id', $data->form_input_checkbox_item_id)->first();
                                        if($value['form_input_checkbox_id'] == $data->form_input_checkbox_id) {
                                            if(!in_array($value['name'],$value_array)) {
                                                array_push($value_array, $value['name']);
                                            }
                                        }
                                    } else {
                                        array_push($value_array, '');
                                    }
                                }
                                break;
                            default:

                                array_push($value_array, '');
                                break;
                        }
                    }


                }

            }
        }

        array_push($var_array, 'signature');
        array_push($var_array, 'now');
        if($client_form->signed == '1') {
            array_push($value_array, $user->first_name.' '.$user->last_name);
            array_push($value_array, date('Y-m-d'));
        }else {
            array_push($value_array, '');
            array_push($value_array, '');
        }
//dd($value_array);
        $templateProcessor->setValue(
            $var_array,$value_array
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedforms/'.($client->company != null ? str_replace(' ','_',str_replace('(','_',str_replace(')','_',$client->company))) : $client->first_name.'_'.$client->last_name).'/';
        if (!File::exists(storage_path('app/forms/' . $processed_template_path))) {
            Storage::makeDirectory('forms/' . $processed_template_path);
        }
        $filename = explode('.',$template_file);

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',$filename[0]). "_" . ($client->company != null ? str_replace(' ','_',str_replace('(','_',str_replace(')','_',$client->company))) : $client->first_name.'_'.$client->last_name) . ".docx";
        if(File::exists(storage_path('app/forms/' . $processed_template))){
            Storage::delete('forms/' . $processed_template);
        }

        $processed_template_pdf = $processed_template_path . DIRECTORY_SEPARATOR . str_replace(' ','_',$filename[0]). "_" . ($client->company != null ? str_replace(' ','_',str_replace('(','_',str_replace(')','_',$client->company))) : $client->first_name.'_'.$client->last_name) . ".pdf";

        if(File::exists(storage_path('app/forms/' . $processed_template_pdf))){
            Storage::delete('forms/' . $processed_template_pdf);
        }
        $templateProcessor->saveAs(storage_path('app/forms/' . $processed_template));
        //dd(storage_path('app/forms/' .$processed_template));
        shell_exec('libreoffice --headless --convert-to pdf '.storage_path('app/forms/' .$processed_template).' --outdir '.storage_path('app/forms/' . $processed_template_path));
        return Storage::download('forms/' . $processed_template_pdf);

    }

    public function processPdf($client_id,$form_id){
        $client = Client::find($client_id);

        $client_form = ClientForm::where('id',$form_id)->where('client_id',$client_id)->first();

        $template_file = Forms::withTrashed()->where('id',$client_form->dynamic_form_id)->first()->file;

        $formfields = FormSection::with(['form_section_input.input.data'=>function ($q) use ($client_id){
            $q->where('client_id',$client_id);
        }])->where('form_id',$client_form->dynamic_form_id)->get();

        if($client_form->signed == '1') {
            $user = User::where('id',$client_form->signed_by)->first();
        }
        /*$templateProcessor = new TemplateProcessor(storage_path('app/forms/templates/' . $template_file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $var_array = array();
        $value_array = array();
        //dd($formfields);
        foreach($formfields as $sections) {
            foreach ($sections["form_section_input"] as $section) {
                $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                array_push($var_array,$var);
                if($client_form->signed == '1') {
                    array_push($var_array, 'signature');
                    array_push($var_array, 'now');
                }
                if(isset($section["input"]->data) && count($section["input"]->data) > 0) {
                    foreach ($section["input"]->data as $value) {
                        array_push($value_array, $value->data);
                        if($client_form->signed == '1') {
                            array_push($value_array, $user->first_name . ' ' . $user->last_name);
                            array_push($value_array, date('Y-m-d'));
                        }
                    }
                } else {
                    array_push($value_array, '');
                    if($client_form->signed == '1') {
                        array_push($value_array, $user->first_name . ' ' . $user->last_name);
                        array_push($value_array, date('Y-m-d'));
                    }
                }

            }
        }

        $templateProcessor->setValue(
            $var_array,$value_array
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedforms/'.($client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'/';
        if (!File::exists(storage_path('app/forms/' . $processed_template_path))) {
            Storage::makeDirectory('forms/' . $processed_template_path);
        }
        $filename = explode('.',$template_file);

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . $filename[0]. "_" . ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name) . ".docx";

        Storage::delete('forms/' . $processed_template);


        $templateProcessor->saveAs(storage_path('app/forms/' . $processed_template));

        return Storage::download('forms/' . $processed_template);*/

        return response()->download(storage_path('app/forms/templates/'.$template_file));
    }

    public function signDynamicForm($client_id, $form_id)
    {

        $client = Client::find($client_id);

        $user = User::where('id',Auth::id())->first();

        $client_form = ClientForm::where('id',$form_id)->first();

        $template_file = Forms::withTrashed()->where('id',$client_form->dynamic_form_id)->first()->file;

        $ext = pathinfo($template_file, PATHINFO_EXTENSION);

        //Only process docx files for now
        if ($ext != "docx") {
            return $template_file;
        }

        $formfields = FormSection::with('form_section_input.input.data')->where('form_id',$client_form->dynamic_form_id)->get();

        $templateProcessor = new TemplateProcessor(storage_path('app/forms/templates/' . $template_file));
        $templateProcessor->setValue('date', date("Y/m/d"));
        $var_array = array();
        $value_array = array();
        foreach($formfields as $sections) {
            foreach ($sections["form_section_input"] as $section) {
                $var = strtolower(str_replace(' ', '_', $sections->name)) . '.' . $section["order"];
                array_push($var_array,$var);
                array_push($var_array,'signature');
                array_push($var_array,'now');
                if(count($section["input"]->data) > 0) {
                    foreach ($section["input"]->data as $value) {
                        array_push($value_array, $value->data);
                        array_push($value_array,$user->first_name.' '.$user->last_name);
                        array_push($value_array,date('Y-m-d'));
                    }
                } else {
                    array_push($value_array, '');
                    array_push($value_array,$user->first_name.' '.$user->last_name);
                    array_push($value_array,date('Y-m-d'));
                }

            }
        }

        $sign_form = ClientForm::find($client_form->id);
        $sign_form->signed = '1';
        $sign_form->signed_date = now();
        $sign_form->signed_by = Auth::id();
        $sign_form->save();


        //return $value_array;

        $templateProcessor->setValue(
            $var_array,$value_array
        );

        //Create directory to store processed templates, for future reference or to check what was sent to the client
        $processed_template_path = 'processedforms/'.($client->company != null ? $client->company : $client->first_name.' '.$client->last_name).'/';
        if (!File::exists(storage_path('app/forms/' . $processed_template_path))) {
            Storage::makeDirectory('forms/' . $processed_template_path);
        }
        $filename = explode('.',$template_file);

        $processed_template = $processed_template_path . DIRECTORY_SEPARATOR . $filename[0]. "_" . ($client->company != null ? $client->company : $client->first_name.' '.$client->last_name) . ".docx";
        //dd(storage_path('app/forms/' . $processed_template));
        Storage::delete('forms/' . $processed_template);

        $templateProcessor->saveAs(storage_path('app/forms/' . $processed_template));

        return redirect()->route('clients.forms',['client'=>$client_id])->with(['flash_success'=> $sign_form->form_type.' successfully signed.']);

    }

    public function getFormFirstSection($form_id){
        $form = FormSection::where('form_id',$form_id)->orderBy('order')->first()->id;

        return $form;
    }

    public function includeInputInClientBasket(Request $request)
    {
        $activity = FormSectionInputInClientBasket::where('client_id', $request->client_id)->where('input_id', $request->input_id)->first()?? new FormSectionInputInClientBasket();
        $activity->client_id = $request->client_id;
        $activity->input_id = $request->input_id;
        $activity->form_id = $request->form_id;
        $activity->in_client_basket = $request->status;
        $activity->save();

        $act = FormSectionInputs::find($request->input_id, ['name']);
        if($request->has('all') && $request->input('all') == 1) {
            $message = $request->status ? 'All activities were successfully included in the basket' : 'All activities were successfully removed from the basket';
        } else {
            $message = $request->status ? $act->name . ' included in the basket successfully' : $act->name . ' removed from the basket successfully';
        }
        return response()->json(['success'=> $message]);
    }
}
