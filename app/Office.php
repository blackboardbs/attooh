<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function processes()
    {
        return $this->hasMany('App\Process');
    }
}
