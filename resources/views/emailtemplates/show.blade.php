+
@extends('adminlte.default')

@section('title') View Email Template @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('emailtemplates.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
    @foreach($template as $result)
        <table class="table table-responsive mt-3">
            <tr>
                <td>Name:</td>
                <td>{{$result->name}}</td>
            </tr>
            <tr>
                <td>Email Subject:</td>
                <td>{{$result->email_subject}}</td>
            </tr>
            <tr>
                <td>Email Body</td>
                <td>@php echo $result->email_content @endphp</td>
            </tr>
        </table>

    @endforeach
    </div>
@endsection
@section('extra-js')

@endsection