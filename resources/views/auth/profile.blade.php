@extends('adminlte.default')

@section('title') Profile @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>{{$user->name()}}</h3>
@endsection

@section('content')
    <div class="row mt-3">
        <div class="col-lg-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="{{route('avatar',['q'=>$user->avatar])}}" class="blackboard-avatar blackboard-avatar-profile"/>
                        </div>
                        <div class="col-lg-9">
                            <ul>
                                <dt>
                                    Email
                                </dt>
                                <dd>
                                    {{$user->email}}
                                </dd>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                    <tr>
                        <th>Roles</th>
                    </tr>
                    </thead>
                    @forelse($user->roles as $role)
                        <tr>
                            <td>{{$role->display_name}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td><small class="text-muted">This user is not assigned to any roles</small></td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                    <tr>
                        <th>Divisions</th>
                    </tr>
                    </thead>
                    @forelse($user->divisions as $division)
                        <tr>
                            <td>{{$division->name}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td><small class="text-muted">This user is not assigned to any divisions</small></td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                    <tr>
                        <th>Regions</th>
                    </tr>
                    </thead>
                    @forelse($user->regions as $region)
                        <tr>
                            <td>{{$region->name}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td><small class="text-muted">This user is not assigned to any regions</small></td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                    <tr>
                        <th>Areas</th>
                    </tr>
                    </thead>
                    @forelse($user->areas as $area)
                        <tr>
                            <td>{{$area->name}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td><small class="text-muted">This user is not assigned to any areas</small></td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-bordered">
                    <thead>
                    <tr>
                        <th>Offices</th>
                    </tr>
                    </thead>
                    @forelse($user->offices as $office)
                        <tr>
                            <td>{{$office->name}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td><small class="text-muted">This user is not assigned to any offices</small></td>
                        </tr>
                    @endforelse
                </table>
            </div>

        </div>
    </div>
@endsection
