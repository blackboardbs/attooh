@extends('adminlte.default')

@section('title') Upload Document @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('documents.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />
        {{Form::open(['url' => route('documents.store'), 'method' => 'post','files'=>true])}}

        <div class="form-group mt-3">
            {{Form::label('name', 'Name')}}
            {{Form::text('name',old('name'),['class'=>'form-control'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
            @foreach($errors->get('name') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>

        <div class="form-group">
            {{Form::label('file', 'File')}}
            {{Form::file('file',['class'=>'form-control'. ($errors->has('file') ? ' is-invalid' : ''),'placeholder'=>'File'])}}
            @foreach($errors->get('file') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>

        @if(request()->has('client'))
            {{Form::hidden('client',request()->input('client'))}}
        @endif

        <div class="form-group">
            <button type="submit" class="btn btn-sm">Save</button>
        </div>

        {{Form::close()}}
    </div>
@endsection