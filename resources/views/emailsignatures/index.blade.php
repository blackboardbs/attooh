@extends('layouts.app')

@section('title') Email Signatures @endsection

@section('header')
    <h1><i class="fa fa-pencil-square-o"></i> @yield('title')</h1>
    <a href="{{route('emailsignatures.create')}}" class="btn btn-outline-light float-right"><i class="fa fa-plus"></i> Email Signature</a>
@endsection

@section('content')
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','company'=>'Branch'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="thead-light">
            <tr>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody class="blackboard-locations">
            @foreach($signature as $result)
                <tr>
                    <td><a href="{{route('emailsignatures.show',$result)}}">{{$result->name}}</a></td>
                    <td>
                        <a href="{{route('emailsignatures.edit',$result)}}">Edit</a>
                        {{ Form::open(['method' => 'DELETE','route' => ['emailsignatures.destroy','id'=>$result],'style'=>'display:inline']) }}
                        <a href="#" class="delete deleteDoc">Delete</a>
                        {{Form::close() }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
